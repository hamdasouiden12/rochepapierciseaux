#include <stdio.h>
#include <assert.h>
#include "jeux.h"


int main(){
    for (int i = 0; i < 5; i++){
        char result = hasard();
        assert(result == 'R' || result == 'P' || result == 'C');
    }
    printf("test unitaire pour hasard réussi.\n");
    return 0;
}
